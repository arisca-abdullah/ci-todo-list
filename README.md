# Codeigniter Todo List

> Simple Todo List App with Codeigniter

## How to Use

1. Clone this repo into your machine:

```bash
git clone https://gitlab.com/arisca-abdullah/ci-todo-list.git
```

2. Change to project directory:

```bash
cd ci-todo-list
```

2. Import sql inside folder `database` into your local database server(mysql).

3. Run Codeginiter App on port 8080:

```bash
php -S localhost:8080
```

4. Open browser on [http://localhost:8080](http://localhost:8080)
