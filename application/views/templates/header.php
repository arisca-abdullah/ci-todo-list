<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    $navs = [
        ['title' => 'Home', 'link' => base_url()],
        ['title' => 'About', 'link' => base_url('about')],
    ];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Todo List</title>

    <!-- Materialize CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/materialize.min.css') ?>">
    <!-- My CSS -->
    <link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
</head>
<body>
    <div class="navbar-fixed">
        <nav class="deep-purple">
            <div class="nav-wrapper">
                <div class="container">
                    <a href="<?= base_url() ?>" class="brand-logo">Todo List</a>
                    <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                    <ul class="right hide-on-med-and-down">
                        <?php foreach($navs as $nav): ?>
                            <?php if(isset($page) && $page == $nav['title']): ?>
                                <li class="active"><a href="<?= $nav['link'] ?>"><?= $nav['title'] ?></a></li>
                            <?php else: ?>
                                <li><a href="<?= $nav['link'] ?>"><?= $nav['title'] ?></a></li>
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>    

    <ul class="sidenav" id="mobile-demo">
        <li class="deep-purple"><a class="subheader white-text">Menu</a></li>
        <?php foreach($navs as $nav): ?>
            <?php if(isset($page) && $page == $nav['title']): ?>
                <li class="active"><a href="<?= $nav['link'] ?>"><?= $nav['title'] ?></a></li>
            <?php else: ?>
                <li><a href="<?= $nav['link'] ?>"><?= $nav['title'] ?></a></li>
            <?php endif ?>
        <?php endforeach ?>
    </ul>