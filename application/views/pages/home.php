<div class="container">
    <h1>Todo List</h1>
    
    <form action="<?= base_url('todo/add') ?>" method="post">
        <div class="input-field">
            <i class="material-icons prefix">add_box</i>
            <input id="todo" name="todo" type="text" class="validate">
            <label for="todo">Add Todo</label>
        </div>
    </form>

    <?php if(count($todos) < 1): ?>
        <p class="center-align text-gray">No Data!</p>
    <?php endif ?>

    <table class="highlight mt-m">
        <?php foreach($todos as $todo): ?>
        <tr>
            <?php if($todo['completed'] == true): ?>
                <td class="line-through"><?= $todo['title'] ?></td>
            <?php else: ?>
                <td><?= $todo['title'] ?></td>
            <?php endif ?>
            <td class="right-align">
                <?php if($todo['completed'] == true): ?>
                    <a href="<?= base_url('todo/unmark/' . $todo['id']) ?>" class="btn-floating btn-small waves-effect waves-light light-blue" title="Mark Uncomplete"><i class="material-icons">remove</i></a>
                <?php else: ?>
                    <a href="<?= base_url('todo/mark/' . $todo['id']) ?>" class="btn-floating btn-small waves-effect waves-light light-blue" title="Mark as Complete"><i class="material-icons">check</i></a>
                <?php endif ?>
                <a href="<?= base_url('todo/delete/' . $todo['id']) ?>" class="btn-floating btn-small waves-effect waves-light red" title="Delete"><i class="material-icons">delete</i></a>
            </td>
        </tr>
        <?php endforeach ?>
    </table>

</div>