<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todos_model extends CI_Model {

    public function getTodos()
    {
        $this->db->order_by('id', 'DESC');
        return $this->db->get('todos')->result_array();
    }

    public function getTodo($id)
    {
        return $this->db->get_where('todos', ['id' => $id])->row_array();
    }

    public function addTodo($data)
    {
        $this->db->insert('todos', $data);
        return $this->db->affected_rows();
    }

    public function updateTodo($data, $id)
    {
        $this->db->update('todos', $data, ['id' => $id]);
        return $this->db->affected_rows();
    }

    public function deleteTodo($id)
    {
        $this->db->delete('todos', ['id' => $id]);
        return $this->db->affected_rows();
    }

}