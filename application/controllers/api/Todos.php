<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header("Access-Control-Allow-Methods: *");

if($_SERVER['REQUEST_METHOD'] == "OPTIONS") die();

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

use Restserver\Libraries\REST_Controller;

class Todos extends REST_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Todos_model', 'todos');
    }

    public function index_get()
    {
        $todos = $this->todos->getTodos();

        $this->response($todos, REST_Controller::HTTP_OK);
    }

    public function index_post()
    {
        if($this->post('title') !== null) {
            $todo['title'] = htmlspecialchars($this->post('title'));
            $todo['completed'] = false;
    
            $status = $this->todos->addTodo($todo);
    
            if($status) {
                $todo['id'] = $this->db->insert_id();
    
                $this->response($todo, REST_Controller::HTTP_OK);
            } else {
                $this->response(['message' => 'Failed to add todo!'], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response(['message' => 'Please insert the title!'], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

    public function index_patch($id = null)
    {
        $todo = $this->todos->getTodo($id);
        if($todo) {
            if($this->patch('title') !== null) $todo['title'] = htmlspecialchars($this->patch('title'));
            if($this->patch('completed') !== null) {
                $todo['completed'] = htmlspecialchars($this->patch('completed'));
            }

            $status = $this->todos->updateTodo($todo, $id);

            if($status) {
                $this->response($todo, REST_Controller::HTTP_OK);
            } else {
                $this->response(['message' => 'Failed to update todo!', 'status' => $status,
                    'todo' => $todo,
                    'completed' => htmlspecialchars($this->patch('completed'))
                ], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response(['message' => 'Data not Found!'], REST_Controller::HTTP_OK);
        }
    }

    public function index_delete($id = null)
    {
        $todo = $this->todos->getTodo($id);
        if($todo) {
            $status = $this->todos->deleteTodo($id);
    
            if($status) {
                $this->response($todo, REST_Controller::HTTP_OK);
            } else {
                $this->response(['message' => 'Failed to delete todo!'], REST_Controller::HTTP_BAD_REQUEST);
            }
        } else {
            $this->response(['message' => 'Data not Found!'], REST_Controller::HTTP_OK);
        }
    }

}
