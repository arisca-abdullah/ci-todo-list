<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Todo extends CI_Controller {
    
    public function __construct() {
        parent::__construct();
		$this->load->model('Todos_model', 'todos');
    }

	public function index()
	{
		$todos = $this->todos->getTodos();

		$this->load->view('templates/header', ['page' => 'Home']);
		$this->load->view('pages/home', ['todos' => $todos]);
		$this->load->view('templates/footer');
	}

    public function add()
    {
        $this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('todo', 'Todo', 'required|trim');

		if($this->form_validation->run()) {
			$todo['title'] = htmlspecialchars($this->input->post('todo'));
			$todo['completed'] = false;

			$this->todos->addTodo($todo);
        }
        
        redirect();
    }

    public function mark($id)
    {
        $this->todos->updateTodo(['completed' => true], $id);

        redirect();
    }

    public function unmark($id)
    {
        $this->todos->updateTodo(['completed' => false], $id);

        redirect();
    }

    public function delete($id)
    {
        $this->todos->deleteTodo($id);

		redirect();
    }

	public function about()
	{
		$this->load->view('templates/header', ['page' => 'About']);
		$this->load->view('pages/about');
		$this->load->view('templates/footer');
	}

	public function not_found()
	{
		$this->load->view('templates/header');
		$this->load->view('pages/not_found');
		$this->load->view('templates/footer');
	}

}